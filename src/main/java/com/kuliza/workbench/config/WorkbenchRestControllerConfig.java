package com.kuliza.workbench.config;

import com.kuliza.workbench.WorkbenchRestController;
import com.kuliza.workbench.util.WorkbenchConstants;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WorkbenchRestControllerConfig implements WebMvcConfigurer {

  @Override
  public void configurePathMatch(PathMatchConfigurer configurer) {
    configurer.addPathPrefix(
        WorkbenchConstants.WORKBENCH_API_PREFIX,
        c -> c.isAnnotationPresent(WorkbenchRestController.class));
  }
}
