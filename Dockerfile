# FROM tomcat:8.5.58-jdk8
# Take the war and copy to webapps of tomcat
# COPY target/*.war /usr/local/tomcat/webapps/


FROM openjdk:8-jdk-alpine
RUN addgroup -S kuliza && adduser -S kuliza -G kuliza
USER kuliza:kuliza
ARG WAR_FILE=./target/*-exec.war
COPY ${WAR_FILE} workbench.war
ENTRYPOINT ["java", "-jar", "-XX:+UseG1GC", "workbench.war"]
